function effectsSelector(state) {
  return state.effects || [];
}

function rocketWidthEffectsItemSelector(state) {
  return state.effects
    .find(function(item) { return item.type === EFFECTS.ROCKET_WIDTH});
}

function ballSpeedEffectsItemSelector(state) {
  return state.effects
    .find(function(item) { return item.type === EFFECTS.BALL_SPEED});
}

function isActiveEffectsSelector(state) {
  const effects = effectsSelector(state);
  return effects.findIndex(function(effect) { return effect.time !== 0; }) > -1;
}

function Store() {
  let state = {};

  let reducer = function reducer() {
    console.warn('Warning: Store: Consider to change default reducer');
    return state;
  }

  let handlers = [];

  this.subscribe = function subscribe(newHandler) {
    let newId = 0;
    handlers.forEach(function(item) {
      newId = Math.max(newId, item.id);
    });
    newId +=1;
    handlers.push({
      id: newId,
      handler: newHandler,
    });

    newHandler(state);

    return function unsubscribe() {
      const i = handlers.findIndex(function(item) {
        return item.id === newId;
      });
      handlers.splice(i, 1);
    };
  }.bind(this);

  this.createState = function createState(rootReducer, initialState) {
    state = Object.assign({}, initialState);
    if (typeof rootReducer === 'function') {
      reducer = rootReducer;
    }
    return this;
  }.bind(this);

  this.getState = function getState() {
    return Object.assign({}, state);
  }.bind(this);

  this.dispatch = function dispatch(action) {
    if (!(action instanceof Promise)) {
      state = Object.assign({}, reducer(state, action));
      sendStateToSubscribers(state);
      return this;
    }
    action
      .then(function(resolvedAction) {
        resolvedAction && dispatch(resolvedAction);
      }.bind(this))
      .catch(function(refusedAction) {
        refusedAction && dispatch(refusedAction);
      }.bind(this));
  }.bind(this);

  function sendStateToSubscribers(newState) {
    handlers.forEach(function(item) {
      item.handler(newState);
    });
  }
}

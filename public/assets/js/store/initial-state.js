/**
 * EffectsItem
 * @typedef {Object} EffectsItem
 * @property {string} type Type of effect
 * @property {number} time Time of effect
 */

const initialState = {
  gameStatus: GAME_STATUSES.INITIAL,
  effects: [],
};

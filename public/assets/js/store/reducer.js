const ACTIONS = {
  CHANGE_GAME_STATUS: 'CHANGE_GAME_STATUS',
  CHANGE_ROCKET_WIDTH_TIME: 'CHANGE_ROCKET_WIDTH_TIME',
  CHANGE_BALL_SPEED: 'CHANGE_BALL_SPEED',
  TICK_EFFECTS_TIME: 'TICK_EFFECTS_TIME',
};

/**
 * Action
 * @typedef {Object} Action
 * @property {string} type Action`s type
 * @property {any} payload
 */

function acChangeGameStatus(status) {
  return {
    type: ACTIONS.CHANGE_GAME_STATUS,
    payload: status,
  };
}

function acChangeRocketWidth(multiplier) {
  return {
    type: ACTIONS.CHANGE_ROCKET_WIDTH_TIME,
    payload: multiplier,
  };
}

function acChangeBallSpeed(multiplier) {
  return {
    type: ACTIONS.CHANGE_BALL_SPEED,
    payload: multiplier,
  };
}

function acTickEffectsTime() {
  return {
    type: ACTIONS.TICK_EFFECTS_TIME,
  }
}

const MAP_PERK_ID = {
  [EFFECTS.ROCKET_WIDTH]: {
    positive: PERK_LARGE_ROCKET,
    negative: PERK_SLIM_ROCKET,
  },
  [EFFECTS.BALL_SPEED]: {
    positive: PERK_FAST_BALL,
    negative: PERK_SLOW_BALL,
  },
};

function getPerkIdForEffectType(effectId, time) {
  const perkData = MAP_PERK_ID[effectId];
  return time > 0 ? perkData.positive : perkData.negative;
}

/**
 * Reducer
 * @param {Object} state State of Store
 * @param {Action} action Action object
 * @returns {Object} New state
 */
function reducer(state, action) {
  const type = action.type;
  const payload = action.payload;

  switch (type) {
    case ACTIONS.CHANGE_GAME_STATUS:
      return  Object.assign({}, state,{ gameStatus: payload });

    case ACTIONS.CHANGE_ROCKET_WIDTH_TIME: {
      const period = payload > 1 ? 5000 : -5000;
      const effect = rocketWidthEffectsItemSelector(state)
        || { type: EFFECTS.ROCKET_WIDTH, time: 0 };
      effect.time += period;
      effect.perkId = getPerkIdForEffectType(effect.type, effect.time);
      const effects = state.effects.slice().filter(function(item) {
        return item.type !== EFFECTS.ROCKET_WIDTH;
      });
      return Object.assign({}, state, {
        effects: [effect].concat(effects),
      });
    }

    case ACTIONS.CHANGE_BALL_SPEED: {
      const period = payload > 1 ? 5000 : -5000;
      const effect = ballSpeedEffectsItemSelector(state)
        || { type: EFFECTS.BALL_SPEED, time: 0 };
      effect.time += period;
      effect.perkId = getPerkIdForEffectType(effect.type, effect.time);
      const effects = state.effects.slice().filter(function(item) {
        return item.type !== EFFECTS.BALL_SPEED;
      });
      return Object.assign({}, state, {
        effects: [effect].concat(effects),
      });
    }

    case ACTIONS.TICK_EFFECTS_TIME: {
      const effects = effectsSelector(state)
        .map(function(effect) {
          let effectTime = effect.time;
          let k = effectTime !== 0
            ? effectTime / Math.abs(effectTime)
            : 0;
          const increment = -1000 * k;
          effectTime += increment;

          if (effectTime === 0) {
            PERKS[effect.perkId].emitEndEvent();
          }

          return Object.assign(effect, {
            time: effectTime,
            perkId: getPerkIdForEffectType(effect.type, effectTime),
          });
        })
        .filter(function(effect) {
          return effect.time !== 0;
        });

      return  Object.assign({}, state, {
        effects: effects,
      });
    }

    default:
      return state;
  }
}

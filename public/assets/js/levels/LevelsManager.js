/**
 * props
 * @property {number} level
 */
function LevelsManager(props) {
  let level = props.level;

  const LEVELS = {
    1: levelOne,
    2: levelTwo,
    3: levelThree,
    4: levelFour,
    5: levelFive,
    6: levelSix,
  };

  const levelsGallery = new LevelsGallery({ levels: LEVELS });

  this.getItems = function getItems() {
    return getLevelItems(level);
  }.bind(this);

  this.nextLevel = function nextLevel() {
    level++;
    if (!LEVELS[level]) {
      level = 1;
      eventEmitter.emit(ALL_LEVELS_ARE_COMPLETED);
    }
    return this;
  }.bind(this);

  this.getCurrentLevel = function getCurrentLevel() {
    return level;
  }.bind(this);

  this.setLevel = function setLevel(value) {
    if (LEVELS[value]) {
      level = value;
    }
  }.bind(this);

  eventEmitter
    .addEventListener(CHANGE_LEVEL, this.setLevel);

  function getLevelItems(levelValue) {
    let brickId = 0;

    return [
      new Rocket({
        id: 'rocket_1',
        cx: 400,
        cy: 575,
        width: 70,
        height: 10,
      }),
      new Ball({
        id: 'ball_1',
        cx: 400,
        cy: 565,
        radius: 5,
        speedX: 0,
        speedY: 0,
      }),
    ]
      .concat(LEVELS[levelValue]
        .reduce(function(acc, row, i) {
          row.forEach((value, j) => {
            if (value > 0) {
              brickId++;
              acc.push(new Brick({
                id: `brick_${brickId}`,
                cx: j * BRICK_SIZE.width + BRICK_SIZE.width / 2,
                cy: i * BRICK_SIZE.height + BRICK_SIZE.height / 2,
                value: value,
                cost: value * 100,
              }));
            }
          });
          return acc;
        }, [])
      );
  }
}

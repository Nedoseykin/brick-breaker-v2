const MODAL_DELAY_MS = 300;

// const PAGE_DELAY_MS = 300;

const FIELD_SIZE = {
  width: 800,
  height: 600,
};

const BRICK_SIZE = {
  width: FIELD_SIZE.width / 10,
  height: 12,
};

const COLORS = {
  white: 'rgba(255, 255, 255, 1)',
  violet: 'rgba(131, 111, 255, 1)',
  darkBlue: 'rgba(30, 144, 255, 1)',
  blue: 'rgba(99, 184, 255, 1)',
  green: 'rgba(0, 205, 102, 1)',
  yellow: 'rgba(255, 215, 0, 1)',
  orange: 'rgba(255, 165, 0, 1)',
  red: 'rgba(255, 48, 48, 1)',
};

const BRICK_COLORS = {
  1: COLORS.violet,
  2: COLORS.darkBlue,
  3: COLORS.blue,
  4: COLORS.green,
  5: COLORS.yellow,
  6: COLORS.orange,
  7: COLORS.red,
}

const PERK_SIZE = {
  width: 30,
  height: 12,
};

const MAX_ROCKET_SPEED = 10;

const ROCKET_ACCELERATION = 0.5;

const ROCKET_CHANGE_WIDTH_SPEED = 3;

const MAX_BALL_SPEED = 7;

const BALL_CHANGE_MAX_AXIS_SPEED = 1;

const MAX_PERK_SPEED = 5;

const PERK_ACCELERATION = 0.1;

const GAME_STATUSES = {
  INITIAL: 'INITIAL',
  ACTIVE: 'ACTIVE',
  PAUSED: 'PAUSED',
  SUCCESS: 'SUCCESS',
  FAIL: 'FAIL',
};

const EFFECTS = {
  ROCKET_WIDTH: 'ROCKET_WIDTH',
  BALL_SPEED: 'BALL_SPEED',
};

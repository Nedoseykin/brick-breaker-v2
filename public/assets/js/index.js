document.addEventListener('DOMContentLoaded', start, false);

const eventEmitter = new EventEmitter();

const store = new Store().createState(reducer, initialState);

function start() {
  new App({
    controller: Controller,
    mainMenu: MainMenu,
    game: Game,
  }).start();
}

function App (props) {
  let mainMenu = null;
  let game = null;
  let controller = null;

  this.start = function start() {
    mainMenu = new props.mainMenu();
    game = new props.game();
    controller = new props.controller();

    eventEmitter
      .addEventListener(TOGGLE_FULL_SCREEN, this.toggleFullscreen)
      .emit(START_APPLICATION_COMPONENTS)
      .emit(SHOW_MAIN_MENU);

    store.dispatch(acChangeGameStatus(GAME_STATUSES.INITIAL));

    return this;
  }.bind(this);

  this.toggleFullscreen = function toggleFullscreen() {
    const fullscreenElement = getFullscreenElement();

    if (!fullscreenElement) {
      document.documentElement
        .requestFullscreen()
        .then(() => {});
    } else {
      if (document.exitFullscreen) {
        document
          .exitFullscreen()
          .then(() => {});
      }
    }
  }.bind(this);
}

function Game() {
  let lives = 10;
  let score = 0;
  let status = GAME_STATUSES.INITIAL;

  const page = new BrickBreakerPage()
    .init(score, lives)
    .show();

  const levelsManager = new LevelsManager({ level: 5 });

  let items = [];

  let isActiveEffects = false;
  let isCountDownActive = false;

  eventEmitter
    .addEventListener(BALL_AT_FIELD_BOTTOM, lifeIsLost)
    .addEventListener(BALL_AT_FIELD_TOP, youWin)
    .addEventListener(CHANGE_COST, changeCost)
    .addEventListener(CHANGE_LIVES, changeLives)
    .addEventListener(MAIN_MENU_STATUS_CHANGED, togglePause)
    .addEventListener(GAME_IS_OVER, stop)
    .addEventListener(START_NEW_GAME, startNewGame)
    .addEventListener(CHANGE_ROCKET_WIDTH, changeRocketWidth)
    .addEventListener(CHANGE_BALL_SPEED, changeBallSpeed);

  requestAnimationFrame(render);

  store.subscribe(function(state) {
    status = state.gameStatus;
    isActiveEffects = isActiveEffectsSelector(state);
  });

  function startNewGame() {
    if (status !== GAME_STATUSES.ACTIVE && status !== GAME_STATUSES.PAUSED) {
      if (lives < 1) {
        lives = 10;
        score = 0;
      }
      items = levelsManager.getItems();
      store.dispatch(acChangeGameStatus(GAME_STATUSES.ACTIVE));
      eventEmitter
        .emit(NEW_GAME_HAS_STARTED, { lives: lives, score: score });
    }
  }

  function stop() {
    store.dispatch(acChangeGameStatus(GAME_STATUSES.FAIL));
  }

  function togglePause(mainMenuStatus) {
    if (status === GAME_STATUSES.ACTIVE && mainMenuStatus === true) {
      store.dispatch(acChangeGameStatus(GAME_STATUSES.PAUSED));
    } else if (status === GAME_STATUSES.PAUSED && mainMenuStatus !== true) {
      store.dispatch(acChangeGameStatus(GAME_STATUSES.ACTIVE));
    }
  }

  function render() {
    if (status === GAME_STATUSES.ACTIVE) {
      items = page.render(items);
    }
    requestAnimationFrame(render);
  }

  function lifeIsLost() {
    changeLives(-1);
  }

  function changeCost(value) {
    score += value;
    if (score < 0) {
      score = 0;
    }
    eventEmitter.emit(COST_IS_CHANGED, score);

    if (score === 0) {
      lifeIsLost();
    }
  }

  function changeLives(value) {
    lives += value;
    eventEmitter.emit(LIVES_IS_CHANGED, {
      lives: lives,
      value: value,
    });

    if (lives < 1) {
      youLose();
    }
  }

  function youWin() {
    store.dispatch(acChangeGameStatus(GAME_STATUSES.SUCCESS));
    eventEmitter
      .emit(YOU_WON, score);
    lives++;
    levelsManager.nextLevel();
  }

  function youLose() {
    store.dispatch(acChangeGameStatus(GAME_STATUSES.FAIL));
    eventEmitter.emit(YOU_LOSE);
  }

  function changeRocketWidth(multiplier) {
    store.dispatch(acChangeRocketWidth(multiplier));
    if (isActiveEffects && !isCountDownActive) {
      startCountDown();
    }
  }

  function changeBallSpeed(multiplier) {
    store.dispatch(acChangeBallSpeed(multiplier));
    if (isActiveEffects && !isCountDownActive) {
      startCountDown();
    }
  }

  function startCountDown() {
    isCountDownActive = true;
    tick();

    function tick() {
      delay(1000)
        .then(function() {
          store.dispatch(acTickEffectsTime());
          if (isActiveEffects) {
            tick();
          } else {
            isCountDownActive = false;
          }
        });
    }
  }
}

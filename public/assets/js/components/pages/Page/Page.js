/**
 * props
 * @property {string} content
 */
function Page(props) {
  this.element = document.createElement('div');
  this.element.setAttribute('class', 'page');
  this.element.innerHTML = props.content;

  // eventEmitter.addEventListener(STOP_APPLICATION_COMPONENTS, this.hide);
}

Page.prototype.show = function show() {
  const root = document.getElementById('app-root');
  const element = this.element;
  element.classList.add('page--hidden');
  root.innerHTML = '';
  root.appendChild(element);
  setTimeout(function() {
    element.classList.remove('page--hidden');
  }, 0);

  return this;
};

/* Page.prototype.hide = function hide() {
  const root = document.getElementById('app-root');
  let element = root.querySelector('.page');
  element.classList.add('page--hidden');
  delay(PAGE_DELAY_MS)
    .then(function() {
      element = root.querySelector('.page');
      root.removeChild(element);
      element.classList.remove('page--hidden');
    });

  return this;
}; */

Page.prototype.getElement = function getElement() {
  return this.element;
};

Page.prototype.setContent = function setContent(content) {
  this.element.innerHTML = content;
  return this;
};

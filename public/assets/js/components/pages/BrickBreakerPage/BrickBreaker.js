function BrickBreakerPage() {
  const page = new Page({
    content: '',
  });

  let canvas = null;
  let effects = [];

  this.show = function show() {
    page.show();
    adjustSize();
    return this;
  }.bind(this);

  // this.hide = function hide() {
  //   page.hide();
  // }.bind(this);

  this.render = function render(items) {
    let newItems = items.slice();

    if (canvas) {
      const ctx = canvas.getContext('2d');
      ctx.clearRect(0, 0, FIELD_SIZE.width, FIELD_SIZE.height);

      if (Array.isArray(newItems)) {
        newItems = newItems
          .map(function(item) {
            return item.render(ctx, items);
          })
          .filter(Boolean);
      }
      displayEffectsInfo(ctx);
    }

    return newItems;
  }.bind(this);

  eventEmitter
    .addEventListener(RESIZE_WINDOW, adjustSize)
    .addEventListener(LIVES_IS_CHANGED, renderLifeValue)
    .addEventListener(COST_IS_CHANGED, renderCostValue)
    .addEventListener(YOU_WON, victory)
    .addEventListener(YOU_LOSE, defeat)
    .addEventListener(ROCKET_FIRED, fired)
    .addEventListener(NEW_GAME_HAS_STARTED, newGameHasStarted);

  const unsubscribe = store.subscribe(function(state) {
    effects = effectsSelector(state);
  });

  this.init = function init(score, lives) {
    const content = `
      <div class="brick-breaker-page__wrapper">
        <div class="brick-breaker-page__info-section">
          <h2 class="brick-breaker-page__info-section-title">Brick Breaker</h2>
          <span class="info-item-wrapper">
            <span id="score">${getScoreContent(score)}</span>
          </span>
          <span class="info-item-wrapper">
            <span id="life-count">${getLifeCounterContent(lives)}</span>
          </span>
        </div>
        <div class="brick-breaker-page__field-section">
          <div id="field-message" class="brick-breaker-page__field-message-box">
          </div>
        </div>
      </div>
    `;
    page.setContent(content);
    canvas = document.createElement('canvas');
    canvas.id = 'field';
    canvas.width = FIELD_SIZE.width;
    canvas.height = FIELD_SIZE.height;
    page
      .getElement()
      .querySelector('.brick-breaker-page__field-section')
      .appendChild(canvas);

    return this;
  }

  function getLifeCounterContent(count) {
    return `Life: ${count}`;
  }

  function getScoreContent(value) {
    return `Score: ${value}`;
  }

  function adjustSize () {
    const REM = parseInt(getComputedStyle(document.documentElement)['font-size']);
    const k = FIELD_SIZE.height / FIELD_SIZE.width;
    const wrapper = document.querySelector('.brick-breaker-page__wrapper');
    if (wrapper) {
      const wrapperRect = wrapper.getBoundingClientRect();
      let width = wrapperRect.width;
      let height = (width - 2*REM) * k + REM;
      const fieldSectionCSSHeight = wrapperRect.height - 9 * REM;
      if (height > fieldSectionCSSHeight) {
        height = fieldSectionCSSHeight;
        width = height / k;
      }

      const fieldSection = document.querySelector('.brick-breaker-page__field-section');
      fieldSection.style.width = `${width}px`;
      fieldSection.style.height = `${height}px`;
    }
  }

  function fired() {
    document
      .getElementById('field-message')
      .classList.add('brick-breaker-page__field-message-box--hidden');
  }

  function renderLifeValue(payload) {
    const className = payload.value > 0 ? 'text--positive' : 'text--negative';
    const lifeCount = document.getElementById('life-count');
    lifeCount.innerHTML = `Life: ${payload.lives}`;
    lifeCount.classList.add('large-text');
    lifeCount.classList.add(className);
    delay(1000).then(function() {
      lifeCount.classList.remove('large-text');
      lifeCount.classList.remove(className);
    });
  }

  function renderCostValue(value) {
    const score = document.getElementById('score');
    score.innerHTML = getScoreContent(value);
    score.classList.add('large-text');
    delay(1000).then(function() {
      score.classList.remove('large-text');
    });
  }

  function renderMessage(content) {
    const elm = document.getElementById('field-message');
    if (!elm.classList.contains('brick-breaker-page__field-message-box--hidden')) {
      elm.classList.add('brick-breaker-page__field-message-box--hidden');
    }
    delay(300)
      .then(() => {
        elm.innerHTML = content;
        if (elm.classList.contains('brick-breaker-page__field-message-box--hidden')) {
          elm.classList.remove('brick-breaker-page__field-message-box--hidden');
        }
      });
  }

  function newGameHasStarted(data) {
    const score = data.score;
    const lives = data.lives;
    renderLifeValue({ lives: lives, value: 1 });
    renderCostValue(score);
    renderMessage(getInitialMessageContent());
  }

  function getInitialMessageContent() {
    return `
      <span>Press Space for start</span>
      <span>Press M for show / hide a menu</span>
    `;
  }

  function getWinMessageContent(score) {
    return `
      <span class="large-text">Congratulations, you won!</span>
      <span class="large-text">Score: ${score}</span>
      <span>Press N to start new game</span>
      <span>Press M to show / hide menu</span>
    `;
  }

  function getLoseMessageContent() {
    return `
      <span class="large-text">Sorry, but you fault...</span>
      <span class="large-text">Try again!</span>
      <span>Press N to start new game</span>
      <span>Press M to show / hide menu</span>
    `;
  }

  function victory(score) {
    renderMessage(getWinMessageContent(score));
  }

  function defeat() {
    renderMessage(getLoseMessageContent());
  }

  function displayEffectsInfo(context) {
    if (!isFilledArray(effects)) {
      return;
    }

    context.textAlign = 'center';
    context.textBaseline = 'middle';
    context.font = 'bold 30px sans-serif';
    const maxWidth = 150;

    effects.forEach(function(item, i) {
      const perkId = item.perkId;
      const time = item.time;
      if (time !== 0) {
        const label = `${PERK_LABELS[perkId].label}:  ${time / 1000} sec`;
        const color = PERK_COLORS[perkId].textFill;
        const textY = FIELD_SIZE.height / 2 - (effects.length - i - 1) * 40;
        const textX = FIELD_SIZE.width / 2;

        context.fillStyle = color;
        context.fillText(label, textX, textY, maxWidth);
      }
    });
  }
}

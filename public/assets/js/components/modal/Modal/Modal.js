function Modal(props) {
  const className = ['modal', props.className].filter(Boolean).join(' ');
  const content = props.content;
  const element = document.createElement('div');
  element.setAttribute('class', className);
  element.innerHTML = content;

  this.show = function show() {
    element.classList.add('modal--hidden');
    document.getElementById('modal-root').appendChild(element);
    setTimeout(() => {
      element.classList.remove('modal--hidden');
    }, 0);

    return this;
  }.bind(this);

  this.remove = function remove() {
    new Promise(function(resolve) {
      element.classList.add('modal--hidden');
      setTimeout(function() {
        resolve();
      }, MODAL_DELAY_MS);
    })
      .then(function() {
        document.getElementById('modal-root').removeChild(element);
        element.classList.remove('modal--hidden');
      })
      .catch(() => {});

    return this;
  }.bind(this);

  this.getElement = function getElement() {
    return element;
  }.bind(this);
}

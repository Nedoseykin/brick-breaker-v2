function Controller () {
  // methods
  this.startListen = function startListen() {
    document.addEventListener('keydown', handleKeyDown, false);
    document.addEventListener('keyup', handleKeyUp, false);
    window.addEventListener('resize', handleResize, false);
  }.bind(this);

  this.stopListen = function stopListen () {
    document.removeEventListener('keydown', handleKeyDown);
    document.removeEventListener('keyup', handleKeyUp);
    window.removeEventListener('resize', handleResize);
  }.bind(this);

  eventEmitter
    .addEventListener(START_APPLICATION_COMPONENTS, this.startListen)
    .addEventListener(GAME_IS_OVER, this.stopListen);

  // handlers
  function handleKeyDown(event) {
    event.stopPropagation();
    event.preventDefault();

    const keyCode = event.keyCode;
    const altKey = event.altKey;

    if (event.isComposing || keyCode === 229) {
      return;
    }

    switch (true) {
      case (keyCode === 13) && altKey:
        eventEmitter.emit(TOGGLE_FULL_SCREEN);
        return;

      // case (keyCode === 76):
      //   eventEmitter.emit(TOGGLE_LEVELS_GALLERY);
      //   return;

      case (keyCode === 77):
        eventEmitter.emit(TOGGLE_MAIN_MENU);
        return;

      case (keyCode === 78):
        eventEmitter.emit(START_NEW_GAME);
        return;

      case (keyCode === 37):
        eventEmitter.emit(MOVE_LEFT);
        return;

      case (keyCode === 39):
        eventEmitter.emit(MOVE_RIGHT);
        return;

      case (keyCode === 32):
        eventEmitter.emit(FIRE);
        return;

      default:
        console.log('key down: ', keyCode);
    }
  }

  function handleKeyUp(event) {
    event.preventDefault();
    event.stopPropagation();

    const keyCode = event.keyCode;

    switch (true) {
      case (keyCode === 37 || keyCode === 39):
        eventEmitter.emit(STOP_MOVING);
        return;

      default:
    }
  }

  function handleResize() {
    eventEmitter.emit(RESIZE_WINDOW);
  }
}

function EventEmitter() {
  this.handlers = {};
}

EventEmitter.prototype.addEventListener = function(event, handler) {
  // console.log('Add event listener: ', event, handler);
  this.handlers[event] = this.handlers[event] || [];
  if (!this.handlers[event].includes(handler)) {
    this.handlers[event].push(handler);
  }
  // console.log('handlers: ', this.handlers);
  return this;
}

EventEmitter.prototype.removeEventListener = function(event, handler) {
  if (Array.isArray(this.handlers[event])
    && this.handlers[event].includes(handler)) {
    this.handlers[event] = this.handlers[event]
      .filter(item => item !== handler);
  }
  return this;
}

EventEmitter.prototype.emit = function(event, value) {
  // console.log('Emit event: ', event, value);
  const eventHandlers = this.handlers[event];
  // console.log('eventHandlers: ', eventHandlers);
  if (Array.isArray(eventHandlers)) {
    eventHandlers.forEach(function(handler) {
      handler(value);
    });
  }
  return this;
}

EventEmitter.prototype.clear = function() {
  this.handlers = {};
  return this;
}

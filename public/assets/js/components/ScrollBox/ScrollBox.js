function ScrollBox(props) {
  const REM = parseInt(
    getComputedStyle(document.documentElement).getPropertyValue('font-size')
  );
  const minHandlerSize = 3 * REM;
  let y = 0;

  const children = props.children;
  const className = props.className;

  const element = document.createElement('div');
  `scroll-box${className ? ` ${className}` : ''}`
    .split(' ')
    .filter(Boolean)
    .forEach(function(item) {
      element.classList.add(item);
    });
  element.innerHTML = `
    <div class="scroll-box__content"></div>
    <div class="scroll-box__vertical-bar">
      <div class="scroll-box__vertical-handler"></div>
    </div>
    <div class="scroll-box__horizontal-bar">
      <div class="scroll-box__horizontal-handler"></div>
    </div>
  `;
  const content = element.querySelector('.scroll-box__content');
  const verticalBar = element.querySelector('.scroll-box__vertical-bar');
  const verticalHandler = element.querySelector('.scroll-box__vertical-handler');
  content.appendChild(children);
  element.addEventListener('wheel', handleWheel, false);
  element.addEventListener('mouseenter', handleMouseEnter, false);
  window.addEventListener('resize', checkHandlersStyle, false);
  verticalHandler.addEventListener('mousedown', verticalHandleHandlerMouseDown, false);
  verticalBar.addEventListener('mousedown', handleVerticalBarMouseDown, false);

  this.getElement = function getElement() {
    return element;
  }.bind(this);

  function handleWheel(e) {
    const deltaX = e.deltaX / Math.abs(e.deltaX) * 30;
    const deltaY = e.deltaY / Math.abs(e.deltaY) * 30;
    content.scrollTop += deltaY;
    content.scrollLeft += deltaX;
    checkHandlersStyle();
  }

  function handleMouseEnter() {
    checkHandlersStyle();
  }

  function verticalHandleHandlerMouseDown(e) {
    e.stopPropagation();
    y = e.clientY;
    verticalBar.classList.add('scroll-box__vertical-bar--active');
    document.addEventListener('mousemove', documentMouseMove, false);
    document.addEventListener('mouseup', documentMouseUp, false);
  }

  function documentMouseUp() {
    verticalBar.classList.remove('scroll-box__vertical-bar--active');
    document.removeEventListener('mousemove', documentMouseMove);
    document.removeEventListener('mouseup', documentMouseUp);
  }

  function documentMouseMove(e) {
    const newY = e.clientY;
    content.scrollTop += newY - y;
    y = newY;
    checkHandlersStyle();
  }

  function handleVerticalBarMouseDown(e) {
    const newY = e.clientY;
    const verticalHandlerHeight = verticalHandler.offsetHeight;
    const deltaY = newY - verticalHandler.getBoundingClientRect().top;
    const sign = deltaY / Math.abs(deltaY);
    content.scrollTop += sign * verticalHandlerHeight;
    checkHandlersStyle();
  }

  function checkHandlersStyle() {
    const elementHeight = element.clientHeight;
    const contentHeight = content.scrollHeight;
    const contentTop = content.scrollTop;
    const verticalBarHeight = verticalBar.offsetHeight;
    const k = elementHeight / contentHeight;
    let verticalHandlerHeight = verticalBarHeight * k;
    verticalHandlerHeight = Math.max(verticalHandlerHeight, minHandlerSize);
    const p = contentTop / contentHeight;
    const verticalHandlerPosition = verticalBarHeight * p;
    verticalHandler.style.height = `${verticalHandlerHeight}px`;
    verticalHandler.style.top = `${verticalHandlerPosition}px`;
  }
}

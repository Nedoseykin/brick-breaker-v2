/**
 * props
 * @property {string} id
 * @property {number} cx
 * @property {number} cy
 * @property {number} width
 * @property {number} height
 */
function Rocket(props) {
  const id = props.id;
  let cx = props.cx;
  let cy = props.cy;
  let width = props.width;
  let targetWidth = width;
  let isWidthChanging = false;
  let height = props.height;
  let speed = 0;
  let maxSpeed = MAX_ROCKET_SPEED;
  let acceleration = ROCKET_ACCELERATION;
  let direction = 0;

  const ROCKET_COLORS = {
    default: COLORS.white,
    positive: COLORS.green,
    negative: COLORS.red,
  };

  let color = ROCKET_COLORS.default;

  const item = new Item({
    render: function(context) {
      context.fillStyle = color;
      context.strokeStyle = color;
      roundedRect(context, cx, cy, width, height, height / 2, true);
    }
  });

  this.render = function render(context) {
    move();
    item.render(context);
    return this;
  }.bind(this);

  this.getData = function getData() {
    return {
      id: id,
      cx: cx,
      cy: cy,
      width: width,
      height: height,
      speed: speed,
      direction: direction,
    };
  }.bind(this);

  eventEmitter
    .addEventListener(MOVE_LEFT, moveLeft)
    .addEventListener(MOVE_RIGHT, moveRight)
    .addEventListener(STOP_MOVING, stopMoving)
    .addEventListener(FIRE, fire)
    .addEventListener(LIVES_IS_CHANGED, livesIsChanged)
    .addEventListener(CHANGE_ROCKET_WIDTH, changeWidth)
    .addEventListener(SET_DEFAULT_ROCKET_WIDTH, changeWidth);

  function moveLeft() {
    if (direction > -1) {
      startMoving(-1);
    }
  }

  function moveRight() {
    if (direction < 1) {
      startMoving(1);
    }
  }

  function startMoving(startDirection) {
    speed = acceleration;
    direction = startDirection;
  }

  function stopMoving() {
    speed = 0;
    direction = 0;
  }

  function move() {
    if (direction === 0) {
      return;
    }

    const halfWidth = width / 2;

    speed = speed + acceleration;
    if (speed > maxSpeed) {
      speed = maxSpeed;
    }

    cx = cx + direction * speed;
    if ((direction === -1)
      && (cx - halfWidth) < 0) {
      cx = halfWidth;
      stopMoving();
    } else if ((direction === 1)
      && (cx + halfWidth) > FIELD_SIZE.width) {
      cx = FIELD_SIZE.width - halfWidth;
      stopMoving();
    }

    eventEmitter.emit(ROCKET_IS_MOVED, {
      cx: cx,
      cy: cy,
      width: width,
      height: height,
    });
  }

  function fire() {
    eventEmitter.emit(ROCKET_FIRED, {
      cx: cx,
      cy: cy,
      speed: speed,
      direction: direction,
    });
  }

  function livesIsChanged(payload) {
    color = payload.value > 0 ? ROCKET_COLORS.positive : ROCKET_COLORS.negative;
    delay(1000)
      .then(function(){ color = ROCKET_COLORS.default; });
  }

  function changeWidth(payload) {
    const multiplier = payload || 1;
    let newWidth = width * payload;
    if (multiplier < 1) {
      newWidth = Math.max(newWidth, props.width / 2);
    } else if (multiplier > 1) {
      newWidth = Math.min(newWidth, props.width * 2);
    } else {
      newWidth = props.width;
    }
    targetWidth = newWidth;
    if (width !== targetWidth && !isWidthChanging) {
      startWidthChanging();
    }
  }

  function startWidthChanging() {
    isWidthChanging = true;
    tick();

    function tick() {
      delay(33)
        .then(function() {
          const delta = targetWidth - width;
          const increment = ROCKET_CHANGE_WIDTH_SPEED * delta / Math.abs(delta);
          let newWidth = width + increment;
          const newDelta = targetWidth - newWidth;
          if (newDelta * delta < 0) {
            newWidth = targetWidth;
          }
          width = newWidth;
          if (width !== targetWidth) {
            tick();
          } else {
            isWidthChanging = false;
          }
        });
    }
  }
}

/**
 * props
 * @property {string} id
 * @property {number} cx
 * @property {number} cy
 * @property {number} radius
 * @property {number} speedX
 * @property {number} speedY
 * @property {number} accelerationX
 * @property {number} accelerationY
 * @property {number} directionX
 * @property {number} directionY
 */
function Ball(props) {
  const id = props.id;
  let cx = props.cx;
  let cy = props.cy;
  let radius = props.radius;
  let speedX = props.speedX || 0;
  let speedY = props.speedY || 0;
  let maxAxisSpeed = getDefaultMaxAxisSpeed();
  let targetMaxAxisSpeed = maxAxisSpeed;
  let isMaxAxisSpeedChanging = false;
  let directionX = props.directionX || 0;
  let directionY = props.directionY || 0;
  let a = calculateA();
  let b = calculateB();

  const item = new Item({
    render: function(context) {
      context.fillStyle = 'rgba(255, 255, 255, 1)';
      context.strokeStyle = 'rgba(255, 255, 255, 1)';
      context.beginPath();
      context.arc(cx, cy, radius, 0, Math.PI * 2);
      context.closePath();
      context.fill();
    },
  });

  this.render = function render(context, items) {
    move(items);
    item.render(context);
    return this;
  }.bind(this);

  this.getData = function getData() {
    return {
      id: id,
      cx: cx,
      cy: cy,
      radius: radius,
      speedX: speedX,
      speedY: speedY,
      directionX: directionX,
      directionY: directionY,
      a: a,
      b: b,
    };
  }.bind(this);

  eventEmitter
    .addEventListener(ROCKET_IS_MOVED, rocketIsMoved)
    .addEventListener(ROCKET_FIRED, fired)
    .addEventListener(CHANGE_BALL_SPEED, changeSpeed)
    .addEventListener(SET_DEFAULT_BALL_SPEED, changeSpeed);

  function move(items) {
    let newPoint = {
      cx: cx + directionX * speedX,
      cy: cy + directionY * speedY,
    };
    newPoint = analyseField(newPoint);
    newPoint = analyseBarriers(items, newPoint);

    cx = newPoint.cx;
    cy = newPoint.cy;

    setAB();
  }

  function analyseField(point) {
    const field = {
      top: radius,
      bottom: FIELD_SIZE.height - radius,
      left: radius,
      right: FIELD_SIZE.width - radius,
    };

    const newPoint = {
      cx: point.cx,
      cy: point.cy,
    };

    let hitHorizontal = false;
    let hitVertical = false;

    if (speedY > 0 && directionY < 0 && point.cy < field.top) {
      newPoint.cy = field.top;
      hitHorizontal = true;
      eventEmitter.emit(BALL_AT_FIELD_TOP);
    } else if (speedY > 0 && directionY > 0 && point.cy > field.bottom) {
      newPoint.cy = field.bottom;
      hitHorizontal = true;
      eventEmitter.emit(BALL_AT_FIELD_BOTTOM);
    }

    if (hitHorizontal) {
      const k = Math.abs(newPoint.cy - cy) / speedY;
      newPoint.cx = cx + directionX * speedX * k;
      directionY *= -1;
    }

    // left
    if (speedX > 0 && directionX < 0 && point.cx < field.left) {
      newPoint.cx = field.left;
      hitVertical = true;
    } else if (speedX > 0 && directionX > 0 && point.cx > field.right) {
      newPoint.cx = field.right;
      hitVertical = true;
    }

    if (hitVertical) {
      const k = Math.abs(newPoint.cx - cx) / speedX;
      newPoint.cy = cy + directionY * speedY * k;
      directionX *= -1;
    }

    return newPoint;
  }

  function analyseBarriers(items, point) {
    const newPoint = {
      cx: point.cx,
      cy: point.cy,
    };

    let topBottomHit = false;
    let leftRightHit = false;

    items.forEach(item => {
      const data = item.getData();
      if (data.id === id) return;

      const barrier = {
        top: data.cy - data.height / 2 - radius,
        bottom: data.cy + data.height / 2 + radius,
        left: data.cx - data.width / 2 - radius,
        right: data.cx + data.width / 2 + radius,
      };

      const crossTop = () => (speedY > 0 && directionY > 0
        && cy <= barrier.top && newPoint.cy > barrier.top);
      const crossBottom = () => (speedY > 0 && directionY < 0
        && cy >= barrier.bottom && newPoint.cy < barrier.bottom);
      const crossLeft = () => (speedX > 0 && directionX > 0
        && cx <= barrier.left && newPoint.cx > barrier.left);
      const crossRight = () => (speedX > 0 && directionX < 0
        && cx >= barrier.right && newPoint.cx < barrier.right);

      if (crossTop()) {
        checkHitX('top');
      } else if (crossBottom()) {
        checkHitX('bottom');
      }

      if (crossLeft()) {
        checkHitY('left');
      } else if (crossRight()) {
        checkHitY('right');
      }

      function checkHitX(surface) {
        const hitX = getHitX(surface);
        if (hitX > barrier.left && hitX < barrier.right) {
          newPoint.cy = barrier[surface];
          newPoint.cx = hitX;
          topBottomHit = true;
          eventEmitter.emit(BALL_HIT, data.id);
          changeSpeed();
        }
      }

      function checkHitY(surface) {
        const hitY = getHitY(surface)
        if (hitY > barrier.top && hitY < barrier.bottom) {
          newPoint.cx = barrier[surface];
          newPoint.cy = hitY;
          leftRightHit = true;
          eventEmitter.emit(BALL_HIT, data.id);
        }
      }

      function getHitX(surface) {
        if (speedX === 0) return cx;

        const deltaY = Math.abs(barrier[surface] - cy);
        const deltaX = speedX / speedY * deltaY;

        return cx + directionX * deltaX;
      }

      function getHitY(surface) {
        if (speedY === 0) return cy;

        const deltaX = Math.abs(barrier[surface] - cx);
        const deltaY = speedY / speedX * deltaX;

        return cy + directionY * deltaY;
      }

      function changeSpeed() {
        if (data.speed || data.speedX) {
          const directedSpeedX = directionX * speedX;
          const directedItemSpeed = data.direction * data.speed;

          const k = 0.3;
          const sum = directedSpeedX + directedItemSpeed * k;
          directionX = sum !== 0 ? sum / Math.abs(sum) : 0;
          speedX = Math.min(Math.abs(sum), maxAxisSpeed);
          const maxBallSpeed = MAX_BALL_SPEED * maxAxisSpeed / getDefaultMaxAxisSpeed();
          speedY = Math.sqrt(Math.pow(maxBallSpeed, 2) - Math.pow(speedX, 2));
        }
      }
    });

    if (topBottomHit) {
      directionY *= -1;
    }

    if (leftRightHit) {
      directionX *= -1;
    }

    return newPoint;
  }

  function rocketIsMoved(rocket) {
    const isSticky = directionX === 0 && directionY === 0;
    if (isSticky) {
      cx = rocket.cx;
      cy = rocket.cy - rocket.height / 2 - radius;
      a = 0;
      b = 0;
    }
  }

  function fired(rocket) {
    if (directionY === 0 && directionX === 0) {
      directionX = rocket.direction || getRandomDirection();
      directionY = -1;
      speedX = maxAxisSpeed * rocket.speed / MAX_ROCKET_SPEED || getRandomAxisSpeed();
      speedY = Math.sqrt(Math.pow(MAX_BALL_SPEED, 2) - Math.pow(speedX, 2));
      setAB();
    }
  }

  function getRandomDirection() {
    const result = getRandomFromRange(1, 10);
    return result % 2 ? 1 : -1;
  }

  function getRandomAxisSpeed() {
    return maxAxisSpeed * getRandomFromRange(1, 5) / 10;
  }

  function calculateA() {
    return lineFunctionA(0, 0, speedX * directionX, speedY * directionY);
  }

  function calculateB() {
    return lineFunctionB(cx, cy, a);
  }

  function setAB() {
    a = calculateA();
    b = calculateB();
  }

  function getDefaultMaxAxisSpeed() {
    return Math.sqrt(Math.pow(MAX_BALL_SPEED, 2) * 0.8);
  }

  function changeSpeed(payload) {
    const multiplier = payload || 1;
    const defaultSpeed = getDefaultMaxAxisSpeed();
    let newSpeed = defaultSpeed * multiplier;
    if (multiplier < 1) {
      newSpeed = Math.max(newSpeed, defaultSpeed / 2);
    } else if (multiplier > 1) {
      newSpeed = Math.min(newSpeed, defaultSpeed * 2);
    } else {
      newSpeed = defaultSpeed;
    }
    targetMaxAxisSpeed = newSpeed;
    if (maxAxisSpeed !== targetMaxAxisSpeed && !isMaxAxisSpeedChanging) {
      startMaxAxisSpeedChanging();
    }
  }

  function startMaxAxisSpeedChanging() {
    isMaxAxisSpeedChanging = true;
    tick();

    function tick() {
      delay(33)
        .then(function() {
          const delta = targetMaxAxisSpeed - maxAxisSpeed;
          const increment = BALL_CHANGE_MAX_AXIS_SPEED * delta / Math.abs(delta);
          let newSpeed = maxAxisSpeed + increment;
          const newDelta = targetMaxAxisSpeed - newSpeed;
          if (newDelta * delta < 0) {
            newSpeed = targetMaxAxisSpeed;
          }
          maxAxisSpeed = newSpeed;
          const k = maxAxisSpeed / getDefaultMaxAxisSpeed();
          const ballSpeed = Math.sqrt(Math.pow(speedX, 2) + Math.pow(speedY, 2));
          const l = ballSpeed / MAX_BALL_SPEED;
          speedX = speedX * k / l;
          speedY = speedY * k / l;
          if (maxAxisSpeed !== targetMaxAxisSpeed) {
            tick();
          } else {
            isMaxAxisSpeedChanging = false;
          }
        })
    }
  }
}

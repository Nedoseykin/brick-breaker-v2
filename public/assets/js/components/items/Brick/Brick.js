/**
 * props
 * @property {string} id
 * @property {number} cx
 * @property {number} cy
 * @property {number} width
 * @property {number} height
 * @property {number} value
 * @property {number} cost
 */
function Brick(props) {
  let id = props.id;
  let cx = props.cx;
  let cy = props.cy;
  let width = props.width || BRICK_SIZE.width;
  let height = props.height || BRICK_SIZE.height;
  let value = props.value || 1;
  let cost = props.cost || 0;

  const item = new Item({
    render: function(context) {
      context.fillStyle = BRICK_COLORS[value];
      context.strokeStyle = BRICK_COLORS[value];
      roundedRect(context, cx, cy, width, height, height / 4, true);
    },
  });

  this.render = function render(context) {
    item.render(context);
    let returnedValue = this;

    if (value <= 0) {
      const perkType = getPerkType();

      returnedValue = new Perk({
        id: `perk_${id.split('_')[1]}`,
        type: perkType,
        cx: cx,
        cy: cy,
      });
    }
    return returnedValue;
  }.bind(this);

  this.getData = function getData() {
    return {
      id: id,
      cx: cx,
      cy: cy,
      width: width,
      height: height,
      value: value,
      cost: cost,
    };
  }.bind(this);

  eventEmitter
    .addEventListener(BALL_HIT, hasBeenHit);

  function hasBeenHit(hitId) {
    if (hitId === id) {
      if (value > 0) {
        value = value - 1;
        if (value === 0) {
          eventEmitter.emit(CHANGE_COST, cost);
        }
      } else {
        value = 0;
      }
    }
  }
}

const PERKS = {
  [PERK_PLUS_ONE_LIFE]: {
    render: plusOneLifeRender,
    emitHitEvent: function() { eventEmitter.emit(CHANGE_LIVES, 1) },
  },
  [PERK_MINUS_ONE_LIFE]: {
    render: minusOneLifeRender,
    emitHitEvent: function() { eventEmitter.emit(CHANGE_LIVES, -1) },
  },
  [PERK_LARGE_ROCKET]: {
    render: largeRocket,
    emitHitEvent: function() { eventEmitter.emit(CHANGE_ROCKET_WIDTH, 2) },
    emitEndEvent: function() { eventEmitter.emit(SET_DEFAULT_ROCKET_WIDTH) },
  },
  [PERK_SLIM_ROCKET]: {
    render: slimRocket,
    emitHitEvent: function() { eventEmitter.emit(CHANGE_ROCKET_WIDTH, 0.5) },
    emitEndEvent: function() { eventEmitter.emit(SET_DEFAULT_ROCKET_WIDTH) },
  },
  [PERK_SLOW_BALL]: {
    render: slowBall,
    emitHitEvent: function() { eventEmitter.emit(CHANGE_BALL_SPEED, 0.5) },
    emitEndEvent: function() { eventEmitter.emit(SET_DEFAULT_BALL_SPEED) },
  },
  [PERK_FAST_BALL]: {
    render: fastBall,
    emitHitEvent: function() { eventEmitter.emit(CHANGE_BALL_SPEED, 2) },
    emitEndEvent: function() { eventEmitter.emit(SET_DEFAULT_BALL_SPEED) },
  },
};

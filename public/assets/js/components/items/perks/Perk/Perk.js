/**
 * PerkProps
 * @typedef {Object} PerkProps
 * @property {string} id
 * @property {string} type
 * @property {number} height
 * @property {number} width
 * @property {number} value
 * @property {number} cost
 * @property {number} cx
 * @property {number} cy
 */

/**
 * @param {PerkProps} props
 */
function Perk(props) {
  const id = props.id;
  let type = props.type;
  const width = props.width || PERK_SIZE.width;
  const height = props.height || PERK_SIZE.height;
  let value = props.value || 1;
  let cost = props.cost || 0;
  let cx = props.cx;
  let cy = props.cy;
  let speed = 0;

  const item = new Item({
    render: function(context) {
      PERKS[type].render(context, { cx: cx, cy: cy });
    },
  });

  this.render = function render(context, items) {
    move(items);
    item.render(context);
    return value > 0 ? this : null;
  }.bind(this);

  this.getData = function getData() {
    return {
      id: id,
      cx: cx,
      cy: cy,
      width: width,
      height: height,
      value: value,
      cost: cost,
    }
  }.bind(this);

  eventEmitter
    .addEventListener(BALL_HIT, hasBeenHit);

  function move(items) {
    const newSpeed = speed + PERK_ACCELERATION;
    speed = Math.min(newSpeed, MAX_PERK_SPEED);

    let newPoint = {
      cx: cx,
      cy: cy + speed,
    };

    newPoint = analyzeField(newPoint);
    newPoint = analyzeBarriers(newPoint, items);

    cx = newPoint.cx;
    cy = newPoint.cy;
  }

  function analyzeField(point) {
    let newPoint = {
      cx: point.cx,
      cy: point.cy,
    };

    if (newPoint.cy + height / 2 > FIELD_SIZE.height) {
      newPoint.cy = FIELD_SIZE.height - height / 2;
      hasBeenLost(id);
    }
    return newPoint;
  }

  function analyzeBarriers(point, items) {
    const newPoint = {
      cx: point.cx,
      cy: point.cy,
    };

    let hit = false;

    items.forEach(item => {
      const data = item.getData();
      if (data.id === id) return;

      const barrier = {
        top: data.cy - data.height / 2 - height / 2,
        left: data.cx - data.width / 2 - width / 2,
        right: data.cx + data.width / 2 + width / 2,
      };

      const crossTop = () => (cy <= barrier.top && newPoint.cy > barrier.top);

      if (crossTop()) {
        checkHitX();
      }

      function checkHitX() {
        if (cx > barrier.left && cx < barrier.right) {
          newPoint.cy = barrier.top;
          hit = true;
          hasBeenHit(id);
          speed = 0;
        }
      }
    });

    return newPoint;
  }

  function hasBeenHit(hitId) {
    if (hitId === id) {
      if (value > 0) {
        value -= 1;
        if (value === 0) {
          PERKS[type].emitHitEvent();
        }
      } else {
        value = 0;
      }
    }
  }

  function hasBeenLost(hitId) {
    if (hitId === id) {
      value -= 1;
    }
  }
}

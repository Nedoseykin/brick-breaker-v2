const  plusOneLifeRender = getPerkRender(PERK_PLUS_ONE_LIFE);

const  minusOneLifeRender = getPerkRender(PERK_MINUS_ONE_LIFE);

const largeRocket = getPerkRender(PERK_LARGE_ROCKET);

const slimRocket = getPerkRender(PERK_SLIM_ROCKET);

const slowBall = getPerkRender(PERK_SLOW_BALL);

const fastBall = getPerkRender(PERK_FAST_BALL);

function getPerkRender(perkId) {
  const label = PERK_LABELS[perkId].label;
  const colors = PERK_COLORS[perkId];

  return function perkRender(context, props) {
    context.textAlign = 'center';
    context.textBaseline = 'middle';
    context.fillStyle = colors.textFill;
    context.font = 'bold 10px sans-serif';
    context.fillText(label, props.cx, props.cy, PERK_SIZE.width);
  }
}

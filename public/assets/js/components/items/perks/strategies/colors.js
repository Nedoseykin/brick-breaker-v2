const PERK_COLORS = {
  [PERK_PLUS_ONE_LIFE]: {
    textFill: COLORS.green,
  },
  [PERK_MINUS_ONE_LIFE]: {
    textFill: COLORS.red,
  },
  [PERK_LARGE_ROCKET]: {
    textFill: COLORS.yellow,
  },
  [PERK_SLIM_ROCKET]: {
    textFill: COLORS.blue,
  },
  [PERK_SLOW_BALL]: {
    textFill: COLORS.orange,
  },
  [PERK_FAST_BALL]: {
    textFill: COLORS.violet,
  }
};

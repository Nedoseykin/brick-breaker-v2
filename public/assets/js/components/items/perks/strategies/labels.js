const PERK_LABELS = {
  [PERK_PLUS_ONE_LIFE]: {
    label: 'Life +1',
  },
  [PERK_MINUS_ONE_LIFE]: {
    label: 'Life -1',
  },
  [PERK_LARGE_ROCKET]: {
    label: 'Size x2',
  },
  [PERK_SLIM_ROCKET]: {
    label: 'Size 1/2',
  },
  [PERK_SLOW_BALL]: {
    label: 'Slow ball',
  },
  [PERK_FAST_BALL]: {
    label: 'Fast ball',
  }
};
/**
 * props
 * @property {function} render - context => {}
 */
function Item(props) {
  this.props = props;
}

Item.prototype.render = function render(context) {
  this.props.render(context);
};

function MainMenu() {
  const root = document.getElementById('modal-root');

  const modal = new Modal({
    className: 'modal--filled-smoke modal--align-center',
    content: `
      <div class="modal-content">
        <h3 class="main-menu__title">Main menu</h3>
        <ul class="main-menu"></ul>
      </div>
    `,
  });

  const menu = modal.getElement().querySelector('.main-menu');

  let gameStatus;

  // methods
  this.toggle = function toggle() {
    if(root.querySelector('.main-menu') === null) {
      this.show();
      eventEmitter.emit(MAIN_MENU_STATUS_CHANGED, true);
    } else {
      this.hide();
      eventEmitter.emit(MAIN_MENU_STATUS_CHANGED, false);
    }

    return this;
  }.bind(this);

  this.show = function show() {
    setItemsToMenu(menu);

    modal.show();
    eventEmitter.emit(MAIN_MENU_STATUS_CHANGED, true);

    return this;
  }.bind(this);

  this.hide = function hide() {
    modal.remove();
    eventEmitter.emit(MAIN_MENU_STATUS_CHANGED, false);

    return this;
  }.bind(this);

  this.hideIfTrue = function hideIfTrue(value) {
    if (value === true) {
      return this.hide();
    }
    return this;
  }.bind(this);

  eventEmitter
    .addEventListener(SHOW_MAIN_MENU, this.show)
    .addEventListener(HIDE_MAIN_MENU, this.hide)
    .addEventListener(TOGGLE_MAIN_MENU, this.toggle)
    .addEventListener(TOGGLE_FULL_SCREEN, toggleFullscreen)
    .addEventListener(START_NEW_GAME, this.hide)
    .addEventListener(LEVELS_GALLERY_STATUS_CHANGED, this.hideIfTrue);
    // .addEventListener(STOP_APPLICATION_COMPONENTS, this.hide);

  const unsubscribe = store.subscribe(function(state) {
    gameStatus = state.gameStatus;
    setItemsToMenu(menu);
  });

  // handlers
  function handleHideMenu() {
    eventEmitter.emit(HIDE_MAIN_MENU);
  }

  function handleStartNewGame() {
    eventEmitter.emit(START_NEW_GAME);
  }

  function handleLevelsGallery() {
    eventEmitter.emit(SHOW_LEVELS_GALLERY);
  }

  function handleToggleFullscreen() {
    eventEmitter.emit(TOGGLE_FULL_SCREEN);
  }

  function handleExit() {
    unsubscribe();
    location.reload();
  }

  // functions
  function setItemsToMenu(menu) {
    menu.innerHTML = '';
    const fullscreenMode = Boolean(getFullscreenElement());
    const screenModeLabel = `${fullscreenMode ? 'Windowed' : 'Fullscreen'} mode (Alt + Enter)`;

    [
      {
        label: 'Start new game (`N`)',
        handler: handleStartNewGame,
        isVisible: gameStatus === GAME_STATUSES.INITIAL
          || gameStatus === GAME_STATUSES.FAIL
          || gameStatus === GAME_STATUSES.SUCCESS,
      },
      {
        label: 'Levels gallery',
        handler: handleLevelsGallery,
        isVisible: gameStatus === GAME_STATUSES.INITIAL
          || gameStatus === GAME_STATUSES.SUCCESS,
      },
      {
        label: screenModeLabel,
        handler: handleToggleFullscreen,
        isVisible: true,
      },
      {
        label: 'Quit menu (`M`)',
        handler: handleHideMenu,
        isVisible: true,
      },
      {
        label: 'Exit game',
        handler: handleExit,
        isVisible: true,
      },
    ].forEach(function(item) {
      if (item.isVisible) {
        const li = document.createElement('li');
        li.setAttribute('class', 'main-menu__item');
        li.innerHTML = item.label;
        li.addEventListener('click', item.handler, false);

        menu.appendChild(li);
      }
    });
  }

  function toggleFullscreen() {
    setTimeout(function() {
      setItemsToMenu(menu);
    }, 100);
  }
}

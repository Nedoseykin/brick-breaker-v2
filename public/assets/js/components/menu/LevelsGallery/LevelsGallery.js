/**
 * @param {Object} props
 * @param {Object} levels
 */
function LevelsGallery({ levels }) {
  const root = document.getElementById('modal-root');

  const modal = new Modal({
    className: 'modal__levels-gallery modal--filled-smoke modal--align-center',
    content: `
      <div class="modal-content">
        <h3 class="levels-gallery__title">Levels gallery</h3>
      </div>
    `,
  });
  const menu = document.createElement('ul');
  menu.classList.add('levels-gallery');
  const scrollBox = new ScrollBox({
    children: menu,
    className: 'levels-gallery__scroll-box',
  });
  modal.getElement()
    .querySelector('.modal-content')
    .appendChild(scrollBox.getElement());

  // const menu = modal.getElement().querySelector('.levels-gallery');
  setItemsToMenu(menu, levels);

  this.show = function show() {
    setItemsToMenu(menu, levels);
    modal.show();
    eventEmitter.emit(LEVELS_GALLERY_STATUS_CHANGED, true);

    return this;
  }.bind(this);

  this.hide = function hide() {
    modal.remove();
    eventEmitter.emit(LEVELS_GALLERY_STATUS_CHANGED, false);

    return this;
  }.bind(this);

  this.toggle = function toggle() {
    if (root.querySelector('.levels-gallery')) {
      return this.hide();
    }
    return this.show();
  }.bind(this);

  this.hideIfTrue = function hideIfTrue(value) {
    if (value === true) {
      return this.hide();
    }
  }.bind(this);

  eventEmitter
    .addEventListener(SHOW_LEVELS_GALLERY, this.show)
    .addEventListener(TOGGLE_LEVELS_GALLERY, this.toggle)
    .addEventListener(MAIN_MENU_STATUS_CHANGED, this.hideIfTrue);

  function setItemsToMenu(menu, items) {
    menu.innerHTML = '';
    Object.keys(items)
      .map(function(key) {
        const level = items[key];
        const li = new LevelsGalleryItem({
          level: level,
          code: parseInt(key),
        }).render().getElement();

        menu.appendChild(li);
      })
  }
}

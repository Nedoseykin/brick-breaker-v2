/**
 * Props
 * @param {array[]} level
 * @param {number} code
 */
function LevelsGalleryItem({ level, code }) {
  const element = document.createElement('li');
  element.classList.add('levels-gallery-item');
  element.innerHTML = `
    <button class="levels-gallery-item__button">
      <canvas
        id="level-${code}"
        class="levels-gallery-item__button-content"
        height="${FIELD_SIZE.height}"
        width="${FIELD_SIZE.width}"
      >
        Level ${code}
      </canvas>
    </button>
  `;
  element
    .querySelector('.levels-gallery-item__button')
    .addEventListener('click', handleClick);

  const canvas = element.querySelector(`#level-${code}`);
  const context = canvas.getContext('2d');

  this.render = function render() {
    level.forEach(function(row, i) {
      row.forEach(function(brick, j) {
        if (brick > 0) {
          context.fillStyle = BRICK_COLORS[brick];
          context.strokeStyle = BRICK_COLORS[brick];
          const width = BRICK_SIZE.width;
          const height = BRICK_SIZE.height;
          const cx = j * width + width / 2;
          const cy = i * height + height / 2;
          roundedRect(context, cx, cy, width, height, height / 4, true);
        }
      });
    });
    return this;
  }.bind(this);

  this.getElement = function getElement() {
    return element;
  }.bind(this);

  this.remove = function remove() {
    element.removeEventListener('click', handleClick);
  }.bind(this);

  eventEmitter
    .addEventListener();

  function handleClick() {
    eventEmitter
      .emit(CHANGE_LEVEL, code)
      .emit(TOGGLE_LEVELS_GALLERY)
      .emit(START_NEW_GAME);
  }
}

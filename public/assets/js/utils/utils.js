function delay(ms) {
  return new Promise(function(resolve) {
    setTimeout(resolve, ms);
  });
}

function getRandomFromRange(min, max) {
  return Math.floor(Math.random() * (max - min + 1) + min);
}

function isFilledArray(value) {
  return Array.isArray(value) && (value.length > 0);
}

/**
 * Calculation of constant `a` in direct line function
 * @param {number} x1 Coordinate x of point 1
 * @param {number} y1 Coordinate y of point 1
 * @param {number} x2 Coordinate x of point 2
 * @param {number} y2 Coordinate y of point 2
 * @returns {number} Constant a
 */
function lineFunctionA(x1, y1, x2, y2) {
  let a = 0;
  if (x1 !== x2) {
    a = (y1 - y2) / (x1 - x2);
  }

  return a;
}

/**
 * Calculation of constant `b` in direct line function
 * @param {number} x Coordinate x
 * @param {number} y Coordinate y
 * @param {number} a Multiplier a
 * @returns {number} Constant b
 */
function lineFunctionB(x, y, a) {
  return y - a * x;
}

function getFullscreenElement() {
  return  document.fullscreenElement
    || document.mozFullScreenElement
    || document.msFullscreenElement
    || document.webkitFullscreenElement;
}

function roundedRect(context, cx, cy, width, height, radius, isFilled) {
  const x = cx - width / 2;
  const y = cy - height / 2;

  context.beginPath();
  context.moveTo(x, y + radius);
  context.lineTo(x, y + height - radius);
  context.arcTo(x, y + height, x + radius, y + height, radius);
  context.lineTo(x + width - radius, y + height);
  context.arcTo(x + width, y + height, x + width, y + height-radius, radius);
  context.lineTo(x + width, y + radius);
  context.arcTo(x + width, y, x + width - radius, y, radius);
  context.lineTo(x + radius, y);
  context.arcTo(x, y, x, y + radius, radius);
  context.closePath();
  context.stroke();
  if (isFilled) {
    context.fill();
  }
}

/**
 * Returns random perk type from the array PERKS_LIST
 * Memoization helps to make equal rate of different perk types
 * @returns {string} Perk type
 */
const getPerkType = function getPerkType() {
  const statistic = {};

  function getPerkList() {
    return PERKS_LIST
      .map(function(perk) {
        return { id: perk, rate: statistic[perk] || 0 };
      })
      .sort(function(perk1, perk2) {
        return perk1.rate - perk2.rate;
      })
      .slice(0, PERKS_LIST.length - 3)
      .map(function(perk) {
        return perk.id;
      });
  }

  return function() {
    const perksList = getPerkList();
    const range = 100 / perksList.length;
    const i = Math.floor(getRandomFromRange(0, 99) / range);
    const perkType = perksList[i];
    if (!statistic[perkType]) {
      statistic[perkType] = 1;
    } else {
      statistic[perkType] +=1;
    }

    return perkType;
  };
}();
